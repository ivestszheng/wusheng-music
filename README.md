<h1 align="center">wusheng-music</h1>

<p align="center">
  <a href=""><img alt="license" src="https://img.shields.io/github/license/Yin-Hongwei/music-website"></a>
</p>

#### 介绍

本音乐网站的开发主要利用 VUE 框架开发前台和后台，后端接口用 Spring Boot + MyBatis 来实现，数据库使用的是 MySQL。原项目是PC端，我做了个移动端，一些功能在基础上做了修改。

#### 系统环境配置

系统开发平台：JDK1.8 + Windows 10 + Maven3.6.1 

开发语言：JavaEE+vue2.X 

后台框架：Springboot2.X 

前端：Vue2.9.6 

数据库和工具：MySql5.7   Navicat  

开发工具： Intellij Idea  VSCode 

浏览器：Chrome

#### 参与贡献

1.  [原创地址](https://github.com/Yin-Hongwei/music-website)
2.  [bilibili 王汉远视频教程](https://www.bilibili.com/video/BV1Ck4y127cg)

#### 演示视频

[bilibili BV12B4y1P7KU](https://www.bilibili.com/video/BV12B4y1P7KU/)

#### 资源文件

[百度云 提取码:cn8w](https://pan.baidu.com/s/1DENA9iyB699FgY58nXLPpQ)

#### 项目截图

<div style="padding:0 auto;">
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/index.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/live.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/mine.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/mine-logined.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/player.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/search.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/singer-list.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/song-list.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/song-list-content.png" style="width:200px;height:355px"></img>
    <img src="https://raw.githubusercontent.com/ivestszheng/images-store/master/wusheng-music/logout.png" style="width:200px;height:355px"></img>
</div>

